# Hello Node

## LYONSCG U Project 1
Davis DeVries

___

### A simple Node server and Express.js app exploring the fundamentals of handling HTTP requests, Cookies, and Routing

____

## Setup

```
git clone https://ddevries_LYONSCG@bitbucket.org/ddevries_LYONSCG/hellonode.git
cd hellonode
npm install
npm start
```
The server should run on localhost:3000

Bake some cookies to click on them and visit their dedicated page.