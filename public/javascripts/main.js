
// format cookie name between 'url/endpoint' format and front-end/client format
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

(function() {
    'use strict';

    let cookieJarContainer = $('.cookie-row-1');
    let cookies = Cookies.get();
    const bakeCookieForm = $('#bakeCookieForm');

    // new cookie form handler
    bakeCookieForm.submit((e) => {
        e.preventDefault();
        let inputData = $('#inputName').val();
        inputData = inputData.replaceAll(' ', '-').toLowerCase();
        $('#inputName').val('');
        $.ajax({
            type: bakeCookieForm.attr('method'),
            url: bakeCookieForm.attr('url'),
            data: {
                "cookie": inputData
            },
            success: (data) => {
                let formatCookie = inputData.replaceAll('-', ' ');
                if ($(`#${inputData}`).length) {
                    let cookieCount = Cookies.get(inputData);
                    $(`#${inputData}`).text(`${formatCookie}: ${cookieCount}`);
                } else {
                    cookieJarContainer.append(`<div id='flex-${inputData}' class='d-flex flex-row justify-content-center' style='width:100%;'> </div>`);
                    let flexContainer = $(`#flex-${inputData}`);
                    flexContainer.append(`<p id='${inputData}' class='cookie-item align-self-center'>${formatCookie}: 1</p>`);
                    flexContainer.append(`<img class='cookie-image align-self-center' style='width:25px; height:25px;' src='images/cookie.png'/>`);
                    cookieJarContainer.append(`<hr style='width:100%'>`);
                }

            },
            error: (data) => {
                console.log('An error occured with POST'); 
                console.log(data);
            }
        });
    });

    // load cookies
    for (var property in cookies) {
        if (cookies.hasOwnProperty(property)) {
            let formatCookie = property.replaceAll('-', ' ');
            cookieJarContainer.append(`<div id='flex-${property}' class='d-flex flex-row justify-content-center' style='width:100%;'> </div>`);
            let flexContainer = $(`#flex-${property}`);
            flexContainer.append(`<p id='${property}' class='cookie-item align-self-center'>${formatCookie}: ${cookies[property]}</p>`);
            flexContainer.append(`<img class='cookie-image align-self-center' style='width:25px; height:25px;' src='images/cookie.png'/>`);
            cookieJarContainer.append(`<hr style='width:100%'>`);
        }
    }
    // button handler; 'eating' the cookies on cookie.ejs
    $('#eatButton').on('click', (e) => {
        $.ajax({
            type: 'POST',
            url: window.location.href,
            success: (data) => {
                console.log(data);
                Cookies.remove(data);
                window.location.href = '/';
            }
        });
    });

    // handler for click on cookie in index.ejs
    $('.cookie-row-1').on('click', (e) => {
        let target = $(e.target);
        if (target.hasClass('cookie-item')) {
            window.location.href = `/${e.target.id}`;
        }
    });

})();