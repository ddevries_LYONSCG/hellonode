const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const getIp = require('ipware')().get_ip;

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Helper Functions //

// https://www.npmjs.com/package/ipware
// Best attempt to get user's (client's) real ip-address while keeping it DRY.
function getClientIp(req) {
    return getIp(req);
}

// https://coderwall.com/p/_g3x9q/how-to-check-if-javascript-object-is-empty
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// https://stackoverflow.com/questions/1144783/how-to-replace-all-occurrences-of-a-string-in-javascript
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


let cookieRoutes = new Map();
let recentPost = {};

app.get('/', (req, res, next) => {
    // force a GET 200; disables caching
    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    res.header("Pragma", "no-cache");
    res.header("Expires", 0);
    
    let specialServerCookie = 'server-chocolate-chip';
    if (isEmpty(req.cookies) || !(specialServerCookie in req.cookies)) {
        res.cookie(specialServerCookie, '1');
        cookieRoutes.set(specialServerCookie, '1');
    } else {
        let count = ++req.cookies[specialServerCookie];
        res.cookie(specialServerCookie, count);
        cookieRoutes.set(specialServerCookie, count);
    }

    for (let cookie in req.cookies) {
        if (!(cookie in cookieRoutes)) {
            let count = req.cookies[cookie];
            cookieRoutes.set(cookie, count);
        }
    }

    res.render('index', {
        method: req.method,
        clientIp: getClientIp(req).clientIp,
        referer: req.headers.referer || 'N/A From Client',
        userAgent: req.headers['user-agent'],
        postData:  JSON.stringify(recentPost)
    });
});

app.post('/', (req, res, next) => {
    recentPost = req.body;

    if ('cookie' in req.body) {
        let cookie = req.body['cookie'];
        cookie.replaceAll(' ', '-').toLowerCase();

        if (!(cookie in req.cookies)) {
            res.cookie(cookie, '1');
            cookieRoutes.set(cookie, '1');
        } else {
            let count = ++req.cookies[cookie];
            res.cookie(cookie, count);
            cookieRoutes.set(cookie, count);
        }
    }

    res.end();
})

app.get('/:name', (req, res, next) => {
    // Force a GET 200; disables caching
    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    res.header("Pragma", "no-cache");
    res.header("Expires", 0);

    console.log(`GET request at ${req.params.name}`);

    let cookieRoute = req.params.name;
    console.log(cookieRoute);

    if (cookieRoutes.has(cookieRoute)) {
        res.render('cookie', {
            name: cookieRoute.replaceAll('-', ' '),
            count: cookieRoutes.get(cookieRoute)
        });
    } else {
        next();
    }
});

app.post('/:name', (req, res, next) => {
    cookieRoutes.delete(req.params.name);
    res.send(req.params.name);
    res.end();
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;